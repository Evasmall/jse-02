package ru.evasmall.tm;

import static ru.evasmall.tm.constant.TerminalConst.*;

/**
 * Приложение вывода параметров к занятию jse-2
 */

public class Main {

    public static void main(final String[] args) {
        run(args);
        displayWelcome();
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length <1) return;
        final String param = args[0];

        switch (param) {
            case CMD_HELP: displayHelp();
            case CMD_ABOUT: displayAbout();
            case CMD_VERSION: displayVersion();
            default: displayError();
        }

    }

    private static void displayWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER! ***");
    }

    private static  void displayError() {
        System.out.println("ERROR! Unknown program argument. ");
        System.exit(-1);
    }

    private static void  displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }

    private static void  displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void  displayAbout() {
        System.out.println("Evgeniya Smolkina");
        System.out.println("smolkina_ev@nlmk.com");
        System.exit(0);
    }

}